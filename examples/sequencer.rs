use pylon::{
    ClosedCamera, GrabOptions, GrabResult, Pylon, Result, SequenceParameterNode, SequenceSetIndex,
    SequencerExt,
};

#[tokio::main]
async fn main() -> Result<()> {
    let pylon = Pylon::new();
    let cam = ClosedCamera::new(&pylon)?;
    let mut cam = cam.open()?;

    let params = &[
        SequenceSetIndex::new(vec![SequenceParameterNode::Float(
            "ExposureTimeAbs",
            1000.0,
        )])
        .with_chunks(&["SequenceSetIndex", "Timestamp"]),
        SequenceSetIndex::new(vec![SequenceParameterNode::Float(
            "ExposureTimeAbs",
            5000.0,
        )])
        .with_chunks(&["SequenceSetIndex", "Timestamp"])
        .with_repetitions(3),
    ];

    cam.init_sequencer(params)?;
    cam.enable_sequencer(true)?;
    let cam = cam.start_grabbing(&GrabOptions::default().count(10))?;

    let mut grab_result = GrabResult::new()?;
    while cam.is_grabbing() {
        cam.retrieve_result(
            5000,
            &mut grab_result,
            pylon_cxx::TimeoutHandling::ThrowException,
        )?;
        let chunk_data = grab_result.chunk_data_node_map()?;
        println!(
            "SequenceSetIndex: {} - Timestamp: {}",
            chunk_data.integer_node("ChunkSequenceSetIndex")?.value()?,
            chunk_data.integer_node("ChunkTimestamp")?.value()?
        );
    }

    cam.stop_grabbing()?.close()?;
    Ok(())
}
