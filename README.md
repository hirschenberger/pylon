# Highlevel API for Basler Pylon cameras

[![Crates.io](https://img.shields.io/crates/v/pylon)](https://crates.io/crates/pylon)
[![docs](https://img.shields.io/badge/documentation-green)](https://hirschenberger.gitlab.io/pylon/doc/pylon/index.html)
[![Crates.io](https://img.shields.io/crates/d/pylon)](https://crates.io/crates/pylon)
[![License](http://img.shields.io/:license-MIT-blue.svg)](http://doge.mit-license.org)


This is a highlevel API on top of the [pylon-cxx](https://github.com/strawlab/pylon-cxx) crate which is a Rust-binding around the official Basler [Pylon-SDK](https://docs.baslerweb.com/pylonapi).

## Features:

* **Typestate-pattern** based camera objects to avoid common mistakes that lead to runtime errors like opening an already open camera or start grabbing on a already grabbing camera.

* Async stream combinators for **Linescan-cameras**.

* Support for the **Sequencer** feature to quickly grab consecutive frames with different acquisition paramters.


```rust
use std::time::Duration;
use pylon::{ClosedCamera, Pylon, LineCameraExt, GrabStatus, PylonError};

let pylon = Pylon::new();

let cam = ClosedCamera::new(&pylon)?;
let mut cam = cam.open()?;    // this is a OpenCamera struct now.

// we use time futures here but it can ba any kind of future (IO-Cards, network-sockets...)
let start = tokio::time::sleep(Duration::from_secs(1));
let end = tokio::time::sleep(Duration::from_secs(2));

assert!(matches!(
    cam.join_from_until(start, end, Some(1024*1024*100)).await?,
    GrabStatus::Success(_)
));
```

Copyright 2024, Falco Hirschenberger <falco.hirschenberger@gmail.com>

