use std::fmt::Display;
use std::future::{ready, Future};

use futures::{pin_mut, select, FutureExt, StreamExt};
use image::{DynamicImage, GrayImage};
use pylon_cxx::{DeviceInfo, InstantCamera, NodeMap, PylonError as PylonCxxError};
use thiserror::Error;

pub use pylon_cxx::GrabOptions;
pub use pylon_cxx::GrabResult;
pub use pylon_cxx::HasProperties;
pub use pylon_cxx::Pylon;
pub use pylon_cxx::TimeoutHandling;

#[derive(Error, Debug)]
pub enum PylonError {
    #[error("Pylon-cxx error")]
    PylonCxx(#[from] PylonCxxError),
    #[error("Pylon error: {0}")]
    Pylon(String),
}

pub type Result<T> = std::result::Result<T, PylonError>;

/// Grab result type to indicate if the full image was grabbed or was trauncated due to size
/// limitations.
#[derive(Debug)]
pub enum GrabStatus {
    /// All images have been grabbes successfully
    Success(DynamicImage),
    /// Images have been grabbed, but the size exceeded the `max_size` parameter and the rest has
    /// been truncated.
    Truncated(DynamicImage),
}

/// Linescan camera async stream combinators.
pub trait LineCameraExt {
    /// Collect and join images till the `end` future resolves.
    ///
    /// If the size if the collected images exceeds the `max_size` parameter, grabbing stops and
    /// the already grabbed images are joined and returned as [GrabStatus::Truncated].
    ///
    /// If the `max_size` parameter is set, the internal buffer reserves this much memory to prevent
    /// reallocations.
    ///  
    /// If `max_size` is [Option::None], grabbing will continue without checks and may exceed your
    /// main memory, leading to serious problems.
    ///
    /// So setting this to a sensible value is a domain specific optimization and a safety
    /// parameter.
    ///
    /// # Example
    ///
    /// ```no_run
    /// # tokio_test::block_on(async{
    ///    use std::time::Duration;
    ///    use pylon::{ClosedCamera, Pylon, LineCameraExt, GrabStatus, PylonError};
    ///
    ///    let pylon = Pylon::new();
    ///    let cam = ClosedCamera::new(&pylon)?;
    ///    let mut cam = cam.open()?;
    ///    let end = tokio::time::sleep(Duration::from_secs(2));
    ///    assert!(matches!(
    ///        cam.join_until(end, Some(1024*1024*100)).await?,
    ///        GrabStatus::Success(_)
    ///    ));
    /// # Ok::<_, PylonError>(())
    /// # });
    /// ```
    ///
    fn join_until<F>(
        &mut self,
        end: F,
        max_size: Option<usize>,
    ) -> impl Future<Output = Result<GrabStatus>>
    where
        F: Future + Send,
        F::Output: std::marker::Send,
    {
        async move { self.join_from_until(ready(()), end, max_size).await }
    }

    /// Start collecting images when the `start` future resolves and join images till the `end`
    /// future resolves.
    ///
    /// If the size if the collected images exceeds the `max_size` parameter, grabbing stops and
    /// the already grabbed images are joined and returned as [GrabStatus::Truncated].
    ///
    /// If the `max_size` parameter is set, the internal buffer reserves this much memory to prevent
    /// reallocations.
    ///  
    /// If `max_size` is [Option::None], grabbing will continue without checks and may exceed your
    /// main memory, leading to serious problems.
    ///
    /// So setting this to a sensible value is a domain specific optimization and a safety
    /// parameter.
    ///
    /// # Example
    ///
    /// ```no_run
    /// # tokio_test::block_on(async{
    ///    use std::time::Duration;
    ///    use pylon::{ClosedCamera, Pylon, LineCameraExt, GrabStatus, PylonError};
    ///
    ///    let pylon = Pylon::new();
    ///    let cam = ClosedCamera::new(&pylon)?;
    ///    let mut cam = cam.open()?;
    ///    let start = tokio::time::sleep(Duration::from_secs(1));
    ///    let end = tokio::time::sleep(Duration::from_secs(2));
    ///    assert!(matches!(
    ///        cam.join_from_until(start, end, Some(1024*1024*100)).await?,
    ///        GrabStatus::Success(_)
    ///    ));
    /// # Ok::<_, PylonError>(())
    /// # });
    /// ```
    fn join_from_until<F, G>(
        &mut self,
        start: F,
        end: G,
        max_size: Option<usize>,
    ) -> impl Future<Output = Result<GrabStatus>>
    where
        F: Future + Send,
        G: Future + Send,
        F::Output: Send,
        G::Output: Send;
}

impl<'a> LineCameraExt for OpenCamera<'a> {
    async fn join_from_until<F, G>(
        &mut self,
        start: F,
        end: G,
        max_size: Option<usize>,
    ) -> Result<GrabStatus>
    where
        F: Future + Send,
        G: Future + Send,
        F::Output: Send,
        G::Output: Send,
    {
        // Use a sensible default capacity. Usually images are rather big, so this avoids
        // some reallocations.
        let mut buff = Vec::with_capacity(max_size.unwrap_or(1024 * 1024));
        let mut width = 0;
        let mut height = 0;

        // start grabbing before waiting for the start-future to minimize delay between start
        // and actually collecting images caused by stating the camera
        self.0.start_grabbing(&GrabOptions::default())?;
        let end = end.fuse();
        pin_mut!(end);

        start.await;

        let truncated = loop {
            select! {
                res = self.0.next().fuse() => {
                    if let Some(res) = res {
                        let img = res.buffer()?;
                        if buff.len() + img.len() <= max_size.unwrap_or(usize::MAX) {
                            width = res.width()?;
                            height += res.height()?;
                            buff.extend_from_slice(img);
                        } else {
                            break true;
                        }
                    } else {
                        self.0.stop_grabbing()?;
                        return Err(PylonError::Pylon("Grabbing a frame failed".into()));
                    }
                },
                _ = end => {
                    self.0.stop_grabbing()?;
                    break false;
                }
            }
        };

        // TODO: Handle all image types
        let img = DynamicImage::ImageLuma8(
            GrayImage::from_vec(width, height, buff)
                .ok_or(PylonError::Pylon("Can't create image from vec".to_string()))?,
        );

        if truncated {
            Ok(GrabStatus::Truncated(img))
        } else {
            Ok(GrabStatus::Success(img))
        }
    }
}

/// Pylon Sequencer paramter.
///
/// These parameters are mapped to a specific camera setting.
///
/// The &str is the name of the basler node. Look up the docs in `pylonviewer` to find the names
/// and datatypes.
///
/// For example:
///
/// ```ignore
/// SequenceParameterNode::Float("ExposureTinmeAbs", 1000.0)
/// ```
#[derive(Debug, Clone)]
pub enum SequenceParameterNode<'a> {
    Boolean(&'a str, bool),
    Integer(&'a str, i64),
    Float(&'a str, f64),
    Enum(&'a str, &'a str),
}

/// A list of these index configs build a sequence of acquisition parameters used to configre the
/// sequencer.
#[derive(Debug, Clone)]
pub struct SequenceSetIndex<'a> {
    nodes: Vec<SequenceParameterNode<'a>>,
    chunks: &'a [&'a str],
    repetitions: u8,
}

impl<'a> SequenceSetIndex<'a> {
    /// Create a new sequencer index step.
    ///
    /// No chunk data will be enables and the repetion count is 1.
    pub fn new(nodes: Vec<SequenceParameterNode<'a>>) -> Self {
        Self {
            nodes,
            chunks: &[],
            repetitions: 1,
        }
    }

    /// Set the number of repetions
    pub fn with_repetitions(self, repetitions: u8) -> Self {
        Self {
            repetitions,
            ..self
        }
    }

    /// Enable a list of chunk-data attached to the [GrabResult] for this sequencer step.
    pub fn with_chunks(self, chunks: &'a [&'a str]) -> Self {
        Self { chunks, ..self }
    }
}

/// Pylon camera Sequencer extension trait
pub trait SequencerExt {
    /// Initialize a Sequencer with the given Parameters.
    ///
    /// The number of [SequenceSetIndex]s define the sequence length, advancing to the next
    /// index each frame (`AutoAdvanceMode`) or when the number of repetions for that index is over,
    /// wrapping around continously.
    ///
    /// To identify the current sequence position, the `ChunkData` `ChunkSequenceSetIndex` can be
    /// enabled and can be queried from [GrabResult] like this:
    ///
    /// ``` ignore
    /// let index = grab_result
    ///     .chunk_data_node_map()?
    ///     .integer_node("ChunkSequenceSetIndex")?
    ///     .value()?;
    /// ```
    fn init_sequencer(&mut self, params: &[SequenceSetIndex]) -> Result<()>;

    /// Enable the Sequencer, it is disabled by default after [SequencerExt::init_sequencer()].
    fn enable_sequencer(&mut self, enabled: bool) -> Result<()>;
}

impl SequencerExt for OpenCamera<'_> {
    fn init_sequencer(&mut self, params: &[SequenceSetIndex]) -> Result<()> {
        let node_map = self.node_map()?;

        // disable the sequencer during configuration
        node_map.boolean_node("SequenceEnable")?.set_value(false)?;

        // enable configuration mode
        node_map
            .enum_node("SequenceConfigurationMode")?
            .set_value("On")?;

        // we only support Auto advance mode yet
        node_map
            .enum_node("SequenceAdvanceMode")?
            .set_value("Auto")?;

        // set the sequence length
        node_map
            .integer_node("SequenceSetTotalNumber")?
            .set_value(params.len() as i64)?;

        for (i, param_index) in params.iter().enumerate() {
            for param in &param_index.nodes {
                match param {
                    SequenceParameterNode::Boolean(name, value) => {
                        node_map.boolean_node(name)?.set_value(*value)?
                    }
                    SequenceParameterNode::Integer(name, value) => {
                        node_map.integer_node(name)?.set_value(*value)?
                    }
                    SequenceParameterNode::Float(name, value) => {
                        node_map.float_node(name)?.set_value(*value)?
                    }
                    SequenceParameterNode::Enum(name, value) => {
                        node_map.enum_node(name)?.set_value(value)?
                    }
                }
            }

            if param_index.repetitions > 1 {
                node_map
                    .integer_node("SequenceSetExecutions")?
                    .set_value(param_index.repetitions as i64)?;
            }

            // enable given chunk datasets in results
            for chunk in param_index.chunks {
                node_map.boolean_node("ChunkModeActive")?.set_value(true)?;
                node_map.enum_node("ChunkSelector")?.set_value(chunk)?;
                node_map.boolean_node("ChunkEnable")?.set_value(true)?;
            }

            // store the parameter in set i
            node_map
                .integer_node("SequenceSetIndex")?
                .set_value(i as i64)?;
            node_map.command_node("SequenceSetStore")?.execute(true)?;
        }

        Ok(())
    }

    fn enable_sequencer(&mut self, enabled: bool) -> Result<()> {
        self.node_map()?
            .boolean_node("SequenceEnable")?
            .set_value(enabled)?;
        Ok(())
    }
}

pub struct ClosedCamera<'a>(InstantCamera<'a>);
pub struct OpenCamera<'a>(pub InstantCamera<'a>);
pub struct GrabbingCamera<'a>(pub InstantCamera<'a>);

impl<'a> ClosedCamera<'a> {
    pub fn new(pylon: &'a Pylon) -> Result<Self> {
        Ok(Self(
            pylon_cxx::TlFactory::instance(pylon).create_first_device()?,
        ))
    }

    pub fn new_by_serial(pylon: &'a Pylon, serial: &str) -> Result<Self> {
        let factory = pylon_cxx::TlFactory::instance(pylon);
        let cam_info = factory
            .enumerate_devices()?
            .into_iter()
            .find(|di| {
                di.property_value("SerialNumber")
                    .expect("query serial number works")
                    == serial
            })
            .ok_or(PylonError::Pylon("Can't find camera: {serial}".to_owned()))?;
        Ok(Self(factory.create_device(&cam_info)?))
    }

    pub fn open(self) -> Result<OpenCamera<'a>> {
        self.0.open()?;
        Ok(OpenCamera(self.0))
    }
}

impl<'a> OpenCamera<'a> {
    pub fn close(self) -> Result<ClosedCamera<'a>> {
        self.0.close()?;
        Ok(ClosedCamera(self.0))
    }

    pub fn node_map<'map>(&'a self) -> Result<NodeMap<'map, 'a>> {
        self.0.node_map().map_err(Into::into)
    }

    pub fn device_info(&'a self) -> DeviceInfo {
        self.0.device_info()
    }

    pub fn start_grabbing(self, options: &GrabOptions) -> Result<GrabbingCamera<'a>> {
        self.0.start_grabbing(options)?;
        Ok(GrabbingCamera(self.0))
    }
}

impl Display for OpenCamera<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let info = self.device_info();
        write!(
            f,
            "name: '{}', serial-number: '{}'",
            info.model_name().unwrap_or("unknown".to_string()),
            info.property_value("SerialNumber")
                .unwrap_or("unknown".to_string()),
        )
    }
}

impl std::fmt::Debug for OpenCamera<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        // just call display
        write!(f, "{}", self)
    }
}

impl<'a> GrabbingCamera<'a> {
    pub fn is_grabbing(&self) -> bool {
        self.0.is_grabbing()
    }

    pub fn retrieve_result(
        &self,
        timeout: u32,
        res: &mut GrabResult,
        timeout_handling: TimeoutHandling,
    ) -> Result<bool> {
        self.0.retrieve_result(timeout, res, timeout_handling)?;
        Ok(true)
    }

    pub fn node_map<'map>(&'a self) -> Result<NodeMap<'map, 'a>> {
        self.0.node_map().map_err(Into::into)
    }

    pub fn stop_grabbing(self) -> Result<OpenCamera<'a>> {
        self.0.stop_grabbing()?;
        Ok(OpenCamera(self.0))
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use pylon_cxx::Pylon;
    use serial_test::serial;
    use std::time::Duration;

    #[tokio::test(flavor = "multi_thread")]
    #[serial]
    async fn test_stream_grab() -> Result<()> {
        let pylon = Pylon::new();
        let cam = ClosedCamera::new(&pylon)?;
        let mut cam = cam.open()?.start_grabbing(&Default::default())?;
        let images: Vec<_> = cam.0.by_ref().take(10).collect().await;
        assert_eq!(images.len(), 10);
        cam.stop_grabbing()?.close()?;
        Ok(())
    }

    #[tokio::test(flavor = "multi_thread")]
    #[serial]
    async fn test_linecam_grab() -> Result<()> {
        let pylon = Pylon::new();
        let cam = ClosedCamera::new(&pylon)?;
        let mut cam = cam.open()?;
        let end = tokio::time::sleep(Duration::from_secs(2));
        if let GrabStatus::Success(image) = cam.join_until(end, None).await? {
            assert!(image.height() > 1024, "Height: {}", image.height());
        }

        Ok(())
    }

    #[tokio::test(flavor = "multi_thread")]
    #[serial]
    async fn test_linecam_grab_limit() -> Result<()> {
        let pylon = Pylon::new();
        let cam = ClosedCamera::new(&pylon)?;
        let mut cam = cam.open()?;

        let start = tokio::time::sleep(Duration::from_secs(1));

        // NOTE: the Pylon cam-emu is ususally grabbing fast enough to exceed the max-size
        // in this time.
        let end = tokio::time::sleep(Duration::from_secs(2));

        assert!(matches!(
            cam.join_from_until(start, end, Some(1024 * 1024 * 5))
                .await?,
            GrabStatus::Truncated(_)
        ));

        Ok(())
    }
}
